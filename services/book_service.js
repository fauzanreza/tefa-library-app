const Joi = require("joi");
const SQLNoRow = require("../exceptions/sql_no_row");
const NotFoundError = require("../exceptions/not_found_error");
const InternalServerError = require("../exceptions/internal_server_error");
const NotImplemented = require("../exceptions/not_implemented");
const BadRequest = require("../exceptions/bad_request");
const BookModel = require("../models/book_model");

class BookService {
  constructor(promiseMysqlPool) {
    this.dbPool = promiseMysqlPool;
  }

  async getOneBook(id) {
    try {
      const connection = await this.dbPool.getConnection();

      const queryResult = await connection.query(
        "SELECT id, judul, sinopsis, jml_hlmn, pengarang, penerbit, tgl_regis, isbn FROM buku WHERE id = ?",
        [id]
      );
      if (queryResult[0].length < 1) {
        throw new SQLNoRow();
      }

      connection.release();

      return queryResult[0][0];
    } catch (err) {
      console.error(err.message);

      let error;

      if (err instanceof SQLNoRow) {
        error = new NotFoundError("Book is not found");
      } else {
        error = new InternalServerError(
          "an error occurred while getting book"
        );
      }

      throw error;
    }
  }

  async getManyBook() {
    try {
      const connection = await this.dbPool.getConnection();

      const queryResult = await connection.query(
        "SELECT id, judul, sinopsis, jml_hlmn, pengarang, penerbit, tgl_regis, isbn FROM buku ORDER BY tgl_regis DESC"
      );
      if (queryResult[0].length < 1) {
        throw new NotFoundError("Book is not found");
      }

      connection.release();

      return queryResult[0];
    } catch (err) {
      console.error(err.message);

      let error;

      if (err instanceof SQLNoRow) {
        error = new NotFoundError("Book is not found");
      } else {
        error = new InternalServerError(
          "an error occurred while getting book"
        );
      }

      throw error;
    }
  }

  async createBook(params) {
    try {
      // validate the request body of new Book.
      await BookModel.getCreateBookModel().validateAsync(params);

      // construct an Book entity, it would be the object that used to store to database.
      const Book = {
        id: null,
        judul: params.judul,
        sinopsis: params.sinopsis,
        jml_hlmn: params.jml_hlmn,
        pengarang: params.pengarang,
        penerbit: params.penerbit,
        tgl_regis: new Date(),
        isbn: params.isbn,
      };

      // get db connection.
      const connection = await this.dbPool.getConnection();

      // execute query, it will run the command to store Book object to db.
      const queryResult = await connection.execute(
        "INSERT INTO buku SET judul = ?, sinopsis = ?, jml_hlmn = ?, pengarang = ?, penerbit = ?, tgl_regis = ?, isbn = ?",
        [Book.judul, Book.sinopsis, Book.jml_hlmn, Book.pengarang, Book.penerbit, Book.tgl_regis, Book.isbn]
      );

      // release the db connection, it will send the unused connection back to the pool.
      connection.release();

      // override the Book id with the new id that returned in query result.
      Book.id = queryResult[0].insertId;

      // return the resolved object that could be the demanded data result.
      return Book;
    } catch (err) {
      // this block will collect the errors if occurred.

      console.error(err.message);

      if (err instanceof Joi.ValidationError) {
        throw new BadRequest(err.message);
      }

      throw new InternalServerError("an error occurred while getting Book");
    }
  }

  async updateBook(id, params) {
    try {
      // 1. validate `params`
      await BookModel.getCreateBookModel().validateAsync(params);
      // 2. check Book with specific `id`, if exist go to number 4, instead go to number 3
      const connection = await this.dbPool.getConnection();

      let queryResult = await connection.execute(
        "SELECT id, judul, sinopsis, jml_hlmn, pengarang, penerbit, tgl_regis, isbn FROM buku WHERE id = ?",
        [id]
      );

      // 3. throw not found error
      if (queryResult[0].length < 1) {
        throw new NotFoundError("Book is not found");
      }

      // 4. construct updatedBook object
      const updatedBook = {
        id: null,
        judul: params.judul,
        sinopsis: params.sinopsis,
        jml_hlmn: params.jml_hlmn,
        pengarang: params.pengarang,
        penerbit: params.penerbit,
        tgl_regis: null,
        isbn: params.isbn,
      };

      // 5. execute update query to ensure the updatedBook object was sent to the database
      queryResult = await connection.query(
        "UPDATE buku SET judul = ?, sinopsis = ?, jml_hlmn = ?, pengarang = ?, penerbit = ?, isbn = ? WHERE id = ?",
        [
          updatedBook.judul,
          updatedBook.sinopsis,
          updatedBook.jml_hlmn,
          updatedBook.pengarang,
          updatedBook.penerbit,
          updatedBook.isbn,
          id,
        ]
      );
      if (queryResult[0].length < 1) {
        throw new NotFoundError("Book is not found");
      }
      connection.release();

      // 6. return updatedBook object
      return updatedBook;
    } catch (err) {
      console.error(err.message);

      if (err instanceof Joi.ValidationError) {
        throw new BadRequest(err.message);
      }
      throw new InternalServerError("an error occurred while updating Book");
    }
  }

  async deleteBook(id) {
    try {
      const connection = await this.dbPool.getConnection();
      // 1. check Book with specific `id`, if exist go to number 3, instead go to number 2
      let queryResult = await connection.execute(
        "SELECT id, judul, sinopsis, jml_hlmn, pengarang, penerbit, isbn FROM buku WHERE id = ?",
        [id]
      );
      // 2. throw not found error
      if (queryResult[0].length < 1) {
        throw new NotFoundError("Book is not found");
      }
      // 3. construct deletedBook object
      const deletedBook = {
        id: null,
        judul: null,
        sinopsis: null,
        jml_hlmn: null,
        pengarang: null,
        penerbit: null,
        tgl_regis: null,
        isbn: null,
      };
      // 4. execute delete query to ensure the data was removed from database's table
      queryResult = await connection.query("DELETE FROM buku WHERE id = ?", [
        id,
      ]);
      if (queryResult[0].length < 1) {
        throw new NotFoundError("Book is not found");
      }
      connection.release();
      // 5. return deletedBook object
      return deletedBook;
    } catch (err) {
      console.error(err.message);

      if (err instanceof Joi.ValidationError) {
        throw new BadRequest(err.message);
      }
      throw new InternalServerError("an error occurred while deleting Book");
    }

    //throw new NotImplemented('delete Book is not implemented');
  }
}

module.exports = BookService;
