const response = require('../responses/responses');

class BookHandler {
  constructor(service) {
    this.service = service;
  }

  async getOneBook(req, res) {
    try {
      const result = await this.service.getOneBook(req.params.id);
      response.success(res, 200, 'OK', 'detail of an book', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async getManyBook(req, res) {
    try {
      const result = await this.service.getManyBook();
      response.success(res, 200, 'OK', 'bunch of books', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async createBook(req, res) {
    try {
      const result = await this.service.createBook(req.body);
      response.success(res, 201, 'CREATED', 'an book has successfuly created', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async updateBook(req, res) {
    const id = req.params.id;
    const update = req.body;

    try {
      const result = await this.service.updateBook(id, update);
      response.success(res, 200, 'MODIFIED', 'an book has successfuly modified', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }

  async deleteBook(req, res) {
    const id = req.params.id;

    try {
      const result = await this.service.deleteBook(id);
      response.success(res, 200, 'DELETED', 'an book has successfuly deleted', result, null);
    } catch (err) {
      response.error(res, err);
    }
  }
}

module.exports = BookHandler;
