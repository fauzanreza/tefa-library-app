const joi = require('joi');

const getCreateBookModel = function(){
  const model = joi.object({
    judul: joi.string().required(),
    sinopsis: joi.string().required(),
    jml_hlmn: joi.number().required(),
    pengarang: joi.string().required(),
    penerbit: joi.string().required(),
    isbn: joi.string().required()
  });

  return model;
}

const getUpdateBookModel = function(){
  const model = joi.object({
    judul: joi.string().required(),
    sinopsis: joi.string().required(),
    jml_hlmn: joi.number().required(),
    pengarang: joi.string().required(),
    penerbit: joi.string().required(),
    isbn: joi.string().required()()
  });

  return model;
}

module.exports = {
  getCreateBookModel: getCreateBookModel,
  getUpdateBookModel: getUpdateBookModel
}
