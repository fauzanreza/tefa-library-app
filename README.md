# tefa-library-app

A simple backend application that mange book information in library

# How to run

- Install Node.js (Latest version is recommended)
- Make sure the Mysql Database is installed first, then create database `tefa_library`
- Fork the repository
- Clone to your local repository on your computer
- Go to the app root directory
- run or import tefa_libray.sql in root directory
- Copy `.env.example` and change to `.env` then populate the enviroment base on your local configuration
- To donwload dependecies run `npm install`
- To start application run `npm start`
